import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import LandingPage from './components/LandingPage'
import ScrollBar from './components/ScrollBar'
import ErrorPage from './components/ErrorPage'
import './App.scss';

const getDocHeight = () => {
  return Math.max(
    document.body.scrollHeight, document.documentElement.scrollHeight,
    document.body.offsetHeight, document.documentElement.offsetHeight,
    document.body.clientHeight, document.documentElement.clientHeight
  );
}

const App = () => {
  const [scrollPosition, setScrollPosition] = useState(0)

  useEffect(() => {
    listenToScrollEvent()
    //eslint-disable-next-line
  }, [])

  const calculateScrollDistance = () => {
    const scrollTop = window.pageYOffset;
    const winHeight = window.innerHeight;
    const docHeight = getDocHeight();

    const totalDocScrollLength = docHeight - winHeight;
    const scrollPostion = Math.floor(scrollTop / totalDocScrollLength * 100);

    setScrollPosition(scrollPostion)
  }

  const listenToScrollEvent = () => {
    document.addEventListener("scroll", () => {
      requestAnimationFrame(() => {
        // Calculates the scroll distance
        calculateScrollDistance();
      });
    });
  };

  return (
    <Router>
      <ScrollBar scroll={scrollPosition} />
      <div className="App">
        <Switch>
          <Route exact path="/" render={(props) => <LandingPage {...props} scroll={scrollPosition} />} />
          <Route component={ErrorPage} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
