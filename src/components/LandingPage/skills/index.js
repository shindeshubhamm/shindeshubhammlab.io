import React from 'react'
import SkillCard from './skillCard'

const Skills = () => {
  return (
    <div className="skills">
      <SkillCard
        title="React JS"
        description="Lorem ipsum dolor, sit amet consectetur adipisicing elit. Blanditiis quam dolore alias laboriosam atque. Corporis reprehenderit assumenda mollitia natus dignissimos."
        experience="1.5 years"
      />
      <SkillCard
        title="Node JS"
        description="Lorem ipsum dolor, sit amet consectetur adipisicing elit. Blanditiis quam dolore alias laboriosam atque. Corporis reprehenderit assumenda mollitia natus dignissimos."
        experience="1.5 years"
      />
      <SkillCard
        title="Next JS"
        description="Lorem ipsum dolor, sit amet consectetur adipisicing elit. Blanditiis quam dolore alias laboriosam atque. Corporis reprehenderit assumenda mollitia natus dignissimos."
        experience="1.5 years"
      />
    </div>
  )
}

export default Skills
