import React from 'react'

const SkillCard = (props) => {
  return (
    <div className="skill-card">
      <h2 className="skill-title">{props.title}</h2>
      <p className="skill-description">{props.description}</p>
      <p className="skill-experience">{props.experience}</p>
    </div>
  )
}

export default SkillCard
