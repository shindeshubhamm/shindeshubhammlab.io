import React from 'react'
import Skills from './skills'
import { RiArrowDropUpLine } from 'react-icons/ri'

const LandingPage = ({ scroll }) => {
  return (
    <div className="landing-page" id="!">
      {/* First Parallex Section */}
      <div className="parallax p-1">
        <div className="content">
          <img src="/assets/logo.svg" alt="Logo" className="logo" />
          <p className="name">shindeshubhamm</p>
        </div>
        <div className="desc">Javascript Developer</div>
        <div className="divider"></div>
        <img src="/assets/avatar.png" alt="shubham" className="photo" />
      </div>

      <section className="section s-1">
        <h1 className="fullname">SHUBHAM SHINDE</h1>
        <p className="profession">Javascript Developer</p>
        <p className="description">About me, Projects, Contact me, Experience, Github Profile</p>
      </section>

      <section className="section s-2">
        <h2 className="section-title">Skills</h2>
        <Skills />
      </section>

      {/* Second Parallex Section */}
      <div className="parallax p-2">
      </div>


      <div className="footer">
        <div className="copyright">
          <span className="details">&copy; 2020 Shubham Shinde.</span>
          <span className="details">All rights reserved.</span>
        </div>
        {scroll >= "20" && <a href="#!" className="scroll-to-top">
          <RiArrowDropUpLine className="scroll-to-top-logo" />
        </a>}
      </div>
    </div>
  )
}

export default LandingPage
