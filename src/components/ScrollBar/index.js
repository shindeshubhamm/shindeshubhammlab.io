import React from 'react'

const styles = (scroll) => {
  return {
    height: '8px',
    backgroundColor: 'orange',
    width: `${scroll}%`,
    position: 'fixed',
    zIndex: '10',
    transition: 'all 0.5s ease'
  }
}

const ScrollBar = ({ scroll }) => {
  return (
    <div style={styles(scroll)}>
    </div>
  )
}

export default ScrollBar
